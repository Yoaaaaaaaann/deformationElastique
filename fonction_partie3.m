function dsol = fonction_partie3(~,sol)

global E I Pl_c Fn_lin_c Ft_lin_c

dsol=zeros(6,1);
dsol(1)=-1+cos(sol(3));
dsol(2)=sin(sol(3));
dsol(3)=sol(4)/(E*I);
dsol(4)=-sol(5);
dsol(5)=-sol(6)*dsol(3)-Pl_c*sin(sol(3))-Fn_lin_c;
dsol(6)=sol(5)*dsol(3)+Pl_c*cos(sol(3))-Ft_lin_c;
end

