clear all
close all

global N E I C0 f Pl_c Fn_lin_c Ft_lin_c

%paramètres géométrique et +
l = 1; %car il faut l>=100b et b=30h
E = 210e9; % Module elastique
b = 0.027; % largeur
h = 0.00094; %epaisseur
I = b*h^3/12; %inertie de section droite de la lame
rho = 7800;
g = 9.81;
C0 = 0;
%C0 = 2*E*I*pi/l; %couple pour une déformée circulaire
Ft = 0;
Fn = 0;
Pl = rho*b*h*g; % poids LINEIQUE selon ex !!!!

P_objet = 0.647; % en Newton (m=66g)
Fn_lin = 0;
Ft_lin = 0;

%déclaration des paramètres
N=200;
delta_s = l/N; %en m
s = 0:delta_s:l;

%% Premier affichage

f_2D = figure('Name','Lame elastique 2D');
f_2D.Position = [900 250 550 400];
 
%affihage2D
%recupération des points expérimentaux via un fichier excel apres pointage
%sur photoshop
[exp,txt,raw] = xlsread('point_exp.xlsx') ;
Y_1 = exp(:,8);
X_1=exp(:,9);

%affichage de la lame droite :
lame_droite(2,1) = 0;
for i = 1:N
    lame_droite(1,i+1) = lame_droite(1,i)+delta_s;
    lame_droite(2,i+1) = 0;
end
plot_lame_droite = plot(lame_droite(2,:),lame_droite(1,:),'Color',[0.5 0.5 0.5],'LineWidth',3);
hold on
    
for incert = -1.5e-2:5e-3:1.5e-2 %incertitudes de mesure (+ ou - 1 cm)
    plot_exp=plot(Y_1,X_1+incert*ones(length(X_1),1),'Color',[0.165 0.867 0.776 0.2] ,'LineWidth',2);
    hold on
end    
    
axis ([-0.8*l 1.2*l -0.6*l 1.4*l])

title("Comparaison entre le modèle numérique et l'expérience")
% Axes : 
xlabel('axe des \overrightarrow{y}', 'Interpreter','latex');
ylabel('axe des \overrightarrow{x}','Interpreter','latex');
grid on

%% NEWTON RAPHSON POIDS+TROUSSE
f= @(M0,Rn0,Rt0) ode45(@fonction_partie3,s,[0,0,0, M0,Rn0,Rt0]);

theta_max = 0; %la déformée initiale est une lame droite
%for h= 0.93e-3:0.001e-3:0.96e-3 %nous faisons varier l'epaisseur de la lame
    I = b*h^3/12; %inertie de section droite de la lame 
    Pl = rho*b*h*g; % poids LINEIQUE 
    d_qqchose = 1e-6; %petit incrément pour calculer les jacobienne

    n=0.01; % facteur multiplicateur initial
    Fn_lin_c = n*Fn_lin; Ft_lin_c = n*Ft_lin; Pl_c = n*Pl;
    Pc_n_c = n*P_objet*sin(theta_max); %projection du poids concentré dans la base t et n
    Pc_t_c = -n*P_objet*cos(theta_max);

    if Fn==0 && C0==0 %perturbation initiale si juste le poids
        Fn=0.05;
    end


    M0 = C0;
    Rn0 = n*Fn*cos(theta_max)+n*Ft*sin(theta_max)+Fn_lin_c*l;
    Rt0 = n*Ft*cos(theta_max)-n*Fn*sin(theta_max)-Pl_c*l + Ft_lin_c*l-n*P_objet;

    for i = n:(1-n)/20:1 % incrément de force

        %mise a jour des conditions initiales en fonction de la déformée
        %précédente
        Fn_lin_c = i*Fn_lin; Ft_lin_c = i*Ft_lin; Pl_c = i*Pl; 
        if i~=n && not((Fn==0 && C0==0))
            xi1_max = new_sol(N+1,1);
            xi2_max = new_sol(N+1,2);
            theta_max = new_sol(N+1,3);
            d1 = l+xi1_max;
            d2 = xi2_max;
            Pc_n_c = i*P_objet*sin(theta_max);
            Pc_t_c = -i*P_objet*cos(theta_max);
            M0 = C0+d1*(i*Fn*cos(theta_max)+i*Ft*sin(theta_max))+d2*(i*Fn*sin(theta_max)-i*Ft*cos(theta_max))+ sum(new_sol(:,2)*Pl_c*delta_s)+ new_sol(N+1,2)*i*P_objet;
            Rn0 = i*Fn*cos(theta_max)+i*Ft*sin(theta_max)+Fn_lin_c*l ;
            Rt0 = i*Ft*cos(theta_max)-i*Fn*sin(theta_max)-Pl_c*l + Ft_lin_c*l - i*P_objet;
        end
        z = [M0;Rn0; Rt0];

        ecart = 1;
        while ecart > 1E-8
     %      définition du systéme à résoudre
            [~, new_sol]= f(M0,Rn0,Rt0);
            f1 = fonction1(M0,Rn0,Rt0,i*Fn,i*P_objet);
            f2 = fonction2(M0,Rn0,Rt0,i*Ft,i*P_objet);
            f3 = fonction3(M0,Rn0,Rt0,C0);
            f_num = [f1; f2; f3];

     %      calcul de la Jacobienne par différentation numérique
             J=Jacobienne(d_qqchose,M0,Rn0,Rt0,f1, f2, f3, i*Fn,i*Ft, i*P_objet);
     %      calcul de l'accroissement et de la nouvelle solution approximée
            dz= -J\f_num;
            z = z+dz;
     %      calcul de l'ecart 
            ecart=norm(dz);
     %      affectation de la nouvelle solution 
            M0 = z(1); Rn0 = z(2); Rt0 = z(3);

        end

        if i>=1-2*(1-n)/20
            %disp(i)
            Fn=0;

        end


    end
    % AFFICHAGE DE LA COURBE COURANTE
    new_position = zeros(2,N+1); %posX / posY
    for p=2:N+1
    new_position(1,p)= new_position(1,p-1)+ delta_s*cos(new_sol(p,3));
    new_position(2,p)= new_position(2,p-1) + delta_s*sin(new_sol(p,3));
    end

    figure(f_2D)
    plot_num = plot(new_position(2,:),new_position(1,:),'Color',[0.8 0.5 0.5],'LineWidth',3);
    hold on
%end


%% AFFICHAGE des legendes et des deformées
%lame exp

%Affichage en 2D
figure(f_2D)
legend([plot_lame_droite plot_exp plot_num],{'Lame droite','Déformation expérimentale poids+trousse','Déformation numérique poids+trousse'})% '\overrightarrow{\xi_1} : deformee en $s = L/2$','\overrightarrow{\xi_2} : deformee en $s = 2L/3$','Interpreter','latex');
  