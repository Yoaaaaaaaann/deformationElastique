function J = Jacobienne(ds,M0,Rn0,Rt0,f1,f2,f3, Fn, Ft,P_objet)
global C0 

f1dM = fonction1(M0+ds,Rn0,Rt0,Fn,P_objet);
f2dM = fonction2(M0+ds,Rn0,Rt0,Ft,P_objet);
f3dM = fonction3(M0+ds,Rn0,Rt0,C0);


f1dRn = fonction1(M0,Rn0+ds,Rt0,Fn,P_objet);
f2dRn = fonction2(M0,Rn0+ds,Rt0,Ft,P_objet);
f3dRn = fonction3(M0,Rn0+ds,Rt0,C0);


f1dRt = fonction1(M0,Rn0,Rt0+ds,Fn,P_objet);
f2dRt = fonction2(M0,Rn0,Rt0+ds,Ft,P_objet);
f3dRt = fonction3(M0,Rn0,Rt0+ds,C0);

J11 = (f1dM-f1)/ds;
J12 = (f1dRn -f1)/ds;
J13 = (f1dRt - f1)/ds;
J21 = (f2dM-f2)/ds;
J22 = (f2dRn-f2)/ds;
J23 = (f2dRt-f2)/ds;
J31 = (f3dM-f3)/ds;
J32 = (f3dRn-f3)/ds;
J33 = (f3dRt-f3)/ds;

J = [J11 J12 J13;
    J21 J22 J23;
    J31 J32 J33];
end

