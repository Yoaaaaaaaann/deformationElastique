function f2 = fonction2(M0,Rn0,Rt0,Ft,P_objet)
    global N f
    %calcul de la déformée courante
    [~,sol] = f(M0,Rn0,Rt0);
    %mise à jour de la nouvelle valeur de theta
    theta_max = sol(N+1,3);
    %calcul du poids concentré courant selon t
    Pc_t_c = -P_objet*cos(theta_max);
    f2 = sol(N+1,6)-Ft-Pc_t_c;
end

