function f1 = fonction1(M0,Rn0,Rt0,Fn,P_objet)
    global N f
    %calcul de la déformée courante
    [~,sol] = f(M0,Rn0,Rt0);
    %mise à jour de la nouvelle valeur de theta
    theta_max = sol(N+1,3);
    %calcul du poids concentré courant selon n
    Pc_n_c = P_objet*sin(theta_max);
    f1 = sol(N+1,5)-Fn-Pc_n_c;
end

