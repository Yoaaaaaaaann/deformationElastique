clear all
close all

global N E I C0 f Pl_c Fn_lin_c Ft_lin_c
%interface pour entrer plus rapidement les valeurs
prompt = {'Entrer une valeur de Couple:','Entrer une valeur de Fn:','Entrer une valeur de Ft:','Entrer une valeur de Fn répartie:','Entrer une valeur de Ft répartie:','Entrer une valeur de masse concentrée (g):','Entrer le nombre de "découpe de force" :','Nombre de points souhaité'};
dlgtitle = 'Entrer la valeur des forces';
dims = [1 35];
definput = {'0.1','0','0','0','0','0','20','200'};
answer = inputdlg(prompt,dlgtitle,dims,definput); 

need_perturbation = false; 

%paramètres géométrique et +
l = 1; %car il faut l>=100b et b=30h
E = 210e9; % Module elastique
b = 0.02; % largeur
h = 0.0005; %epaisseur
I = b*h^3/12; %inertie de section droite de la lame
rho = 7800;
g = 9.81;
C0 = str2double(cell2mat(answer(1))); %couple d'extrémité
%C0 = 2*E*I*pi/l; %couple pour une déformée circulaire

Ft = str2double(cell2mat(answer(3))); % forces d'extrémités
Fn = str2double(cell2mat(answer(2)));

Pl = rho*b*h*g; % poids LINEIQUE selon ex !!!!

P_objet = str2double(cell2mat(answer(6)))*1e-3*9.81; % masse concentrée

Fn_lin = str2double(cell2mat(answer(4))); %forces réparties
Ft_lin = str2double(cell2mat(answer(5)));

%déclaration des paramètres
N=str2double(cell2mat(answer(8)));
delta_s = l/N; %en m
s = 0:delta_s:l;

%% CALCUL DES SOLUTIONS avec juste C0 pour approximer notre solution complète

%création d'une matrice contenant les résultats pour les 6 inconnues
sol=zeros(N+1,6); %sol(1)=ksi1; sol(2)=ksi2; sol(3)=theta; sol(4)=M; sol(5)=Rn; sol(6)=Rt
sol(1,4)=C0;

%Solution utilisant ode45
[s,sol]=ode45(@fonction_partie2,s,sol(1,:));

%% Premier affichage

f_3D = figure('Name','Lame elastique 3D');
f_3D.Position = [100 250 700 400];
f_2D = figure('Name','Lame elastique 2D');
f_2D.Position = [900 250 550 400];
 
%affihage2D
%affichage de la lame droite :
lame_droite(2,1) = 0;
for i = 1:N
    lame_droite(1,i+1) = lame_droite(1,i)+delta_s;
    lame_droite(2,i+1) = 0;
end
plot(lame_droite(2,:),lame_droite(1,:),'Color',[0.5 0.5 0.5],'LineWidth',3)
hold on

axis ([-0.8*l 1.2*l -0.6*l 1.4*l])

title({'Deformation d une lame elastique soumise a un couple $C_0$';'et a des efforts exterieurs  $F_n$, $F_t$, $\widetilde{F_n}$ , $ \widetilde{F_t}$, a $\overrightarrow{P}$ et a une masse concentree'}, 'Interpreter','latex')
% Axes : 
xlabel('axe des \overrightarrow{y}', 'Interpreter','latex');
ylabel('axe des \overrightarrow{x}','Interpreter','latex');


grid on

%affichage 3D
figure(f_3D)

%lame droite :
lame_droite(2,1) = 0;
for i = 1:N
    lame_droite(1,i+1) = lame_droite(1,i)+delta_s;
    lame_droite(2,i+1) = 0;
end

j=1;
for i =0:h/20:h
    plot_3(j) = plot3(lame_droite(2,:),lame_droite(1,:),i*ones(N+1,1),'Color',[0.5 0.5 0.5],'LineWidth',3);
    j=j+1;
    hold on
end
grid on

title({'Deformation d une lame elastique soumise a un couple $C_0$';'et a des efforts exterieurs $F_n$, $F_t$, $\widetilde{F_n}$ , $ \widetilde{F_t}$ et $\overrightarrow{P}$ et a une masse concentree  '}, 'Interpreter','latex')

% Axes : 
xlabel('axe des \overrightarrow{y}', 'Interpreter','latex');
ylabel('axe des \overrightarrow{x}','Interpreter','latex');
zlabel('axe des \overrightarrow{z}','Interpreter','latex');
axis([-0.8*l 1.2*l -0.6*l 1.4*l 0 10*h])
view(10, 50)

%% NEWTON RAPHSON

%fonction renvoyant l'integration de notre modèle et prenant en argument les conditions initiales des forces
f= @(M0,Rn0,Rt0) ode45(@fonction_partie3,s,[0,0,0, M0,Rn0,Rt0]); 

xi1_max = sol(N+1,1);
xi2_max = sol(N+1,2);
theta_max = sol(N+1,3);

%distance entre entre O et la point extreme de la lame : 
d1 = l+xi1_max;  %selon x
d2 = xi2_max; %selon y
d_qqchose = 1e-6; %incrément de M,Rn ou Rt pour la jacobienne

n=0.01; % facteur multiplicateur initial
pas_force = (1-n)/str2double(cell2mat(answer(7))); %pas de découpe de force

Fn_lin_c = n*Fn_lin; Ft_lin_c = n*Ft_lin; Pl_c = n*Pl;

Pc_n_c = n*P_objet*sin(theta_max);
Pc_t_c = -n*P_objet*cos(theta_max);

if Fn==0 && C0==0 %perturbation initiale si juste le poids
    Fn=0.05;
    need_perturbation = true;
end

%calcul des efforts répartis
Mf_lin = 0; %moment des forces réparties
Mp_lin =0; %moment du poids réparti
Rn_lin = 0; %resultante des forces rep selon ey
Rt_lin =0; %resultante des forces rep selon ex
pos=delta_s; %position courante
for k=2:N
    Mf_lin = Mf_lin + (pos+sol(k,1))*(delta_s*Fn_lin_c*cos(sol(k,3))+delta_s*Ft_lin_c*sin(sol(k,3)))+sol(k,2)*(delta_s*Fn_lin_c*sin(sol(k,3))-delta_s*Ft_lin_c*cos(sol(k,3)));
    Mp_lin = Mp_lin + sol(k,2)*Pl_c*delta_s;
    Rn_lin = Rn_lin + Fn_lin_c*delta_s*cos(sol(k,3))+ Ft_lin_c*delta_s*sin(sol(k,3));
    Rt_lin = Rt_lin+ Ft_lin_c*delta_s*cos(sol(k,3))-Fn_lin_c*delta_s*sin(sol(k,3));
    pos=pos+delta_s;
end

M0 = C0+d1*(n*Fn*cos(theta_max)+n*Ft*sin(theta_max))+d2*(n*Fn*sin(theta_max)-n*Ft*cos(theta_max))+sol(N+1,2)*n*P_objet +Mp_lin +Mf_lin;
Rn0 = n*Fn*cos(theta_max)+n*Ft*sin(theta_max)+Rn_lin;
Rt0 = n*Ft*cos(theta_max)-n*Fn*sin(theta_max)-Pl_c*l + Rt_lin -n*P_objet;

for i = n:pas_force:1

    Fn_lin_c = i*Fn_lin; Ft_lin_c = i*Ft_lin; Pl_c = i*Pl; 

    if i~=n && not((Fn==0 && C0==0))
        xi1_max = new_sol(N+1,1);
        xi2_max = new_sol(N+1,2);
        theta_max = new_sol(N+1,3);
        d1 = l+xi1_max;
        d2 = xi2_max;
        Pc_n_c = i*P_objet*sin(theta_max);
        Pc_t_c = -i*P_objet*cos(theta_max);

        Mf_lin = 0;
        Mp_lin =0;
        Rn_lin=0;
        Rt_lin=0;
        pos=delta_s;
        for k=2:N
            Mf_lin = Mf_lin + (pos+new_sol(k,1))*(delta_s*Fn_lin_c*cos(new_sol(k,3))+delta_s*Ft_lin_c*sin(new_sol(k,3)))+new_sol(k,2)*(delta_s*Fn_lin_c*sin(new_sol(k,3))-delta_s*Ft_lin_c*cos(new_sol(k,3)));
            Mp_lin = Mp_lin + new_sol(k,2)*Pl_c*delta_s;
            Rn_lin = Rn_lin + Fn_lin_c*delta_s*cos(new_sol(k,3))+ Ft_lin_c*delta_s*sin(new_sol(k,3));
            Rt_lin = Rt_lin+ Ft_lin_c*delta_s*cos(new_sol(k,3))-Fn_lin_c*delta_s*sin(new_sol(k,3));
            pos=pos+delta_s;
        end

        M0 = C0+d1*(i*Fn*cos(theta_max)+i*Ft*sin(theta_max))+d2*(i*Fn*sin(theta_max)-i*Ft*cos(theta_max))+sol(N+1,2)*i*P_objet+ Mp_lin+Mf_lin;
        Rn0 = i*Fn*cos(theta_max)+i*Ft*sin(theta_max)+Rn_lin ;
        Rt0 = i*Ft*cos(theta_max)-i*Fn*sin(theta_max)-Pl_c*l + Rt_lin- i*P_objet;
    end
    z = [M0;Rn0; Rt0];

    ecart = 1;
    while ecart > 1E-8
 %      définition du systéme à résoudre
        [~, new_sol]= f(M0,Rn0,Rt0);
        f1 = fonction1(M0,Rn0,Rt0,i*Fn,i*P_objet);
        f2 = fonction2(M0,Rn0,Rt0,i*Ft,i*P_objet);
        f3 = fonction3(M0,Rn0,Rt0,C0);
        f_num = [f1; f2; f3];

 %      calcul de la Jacobienne par différentation numérique
         J=Jacobienne(d_qqchose,M0,Rn0,Rt0,f1, f2, f3, i*Fn, i*Ft,i*P_objet);
 %      calcul de l'accroissement et de la nouvelle solution approximée
        dz= -J\f_num;
        z = z+dz;
 %      calcul de l'ecart
        ecart=norm(dz);
 %      affectation de la nouvelle solution 
        M0 = z(1); Rn0 = z(2); Rt0 = z(3);

    end

    if need_perturbation && i >= 1- 10*pas_force %on retire la perturbation  vers la fin
        Fn=0; 
    end

    % AFFICHAGE DE LA COURBE COURANTE
    new_position = zeros(2,N+1); %posX / posY
    for p=2:N+1
        new_position(1,p)= new_position(1,p-1)+ delta_s*cos(new_sol(p,3));
        new_position(2,p)= new_position(2,p-1) + delta_s*sin(new_sol(p,3));
    end

    figure(f_2D)
    plot_4 = plot(new_position(2,:),new_position(1,:),'Color',[0.2 0.8 0.5],'LineWidth',3);

    figure(f_3D)
    j=1;
    for k =0:h/20:h
        plot_2(j) = plot3(new_position(2,:),new_position(1,:),k*ones(N+1,1),'Color',[0.2 0.8 0.5],'LineWidth',3); % on stocke le tracé de chaque courbe
        j=j+1;
    end

    %pause(0.0005);
    if i<=1-pas_force
        delete(plot_2)
        delete(plot_4)
    end


end

%% AFFICHAGE des legendes et des deformées
figure(f_3D)
   
    legend([plot_3(10) plot_2(10)],{'Lame droite','Lame déformée'}); %\overrightarrow{\xi_1} : deformee en $s = L/2$','\overrightarrow{\xi_2} : deformee en $s = 2L/3$','Interpreter','latex');
    annotation('textbox',[.76 .4 .12 .33],'String',strcat('C_0 = ',cell2mat(answer(1)),'      F_n = ',cell2mat(answer(2)),'      F_t = ',cell2mat(answer(3)),'      F_n rep = ',cell2mat(answer(4)),'      F_t rep = ',cell2mat(answer(5))),'EdgeColor',[0 0 0],'Color',[0.08 0.56 0.49],'FontSize',11)

    %Affichage en 2D
figure(f_2D)
    legend('Lame droite','Lame deformée')% '\overrightarrow{\xi_1} : deformee en $s = L/2$','\overrightarrow{\xi_2} : deformee en $s = 2L/3$','Interpreter','latex');
    annotation('textbox',[.75 .4 .14 .33],'String',strcat('C_0 = ',cell2mat(answer(1)),'      F_n = ',cell2mat(answer(2)),'      F_t = ',cell2mat(answer(3)),'      F_n rep = ',cell2mat(answer(4)),'      F_t rep = ',cell2mat(answer(5))),'EdgeColor',[0 0 0],'Color',[0.08 0.56 0.49],'FontSize',11)

