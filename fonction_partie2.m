function dsol = fonction_partie2(s,sol)

global E I

dsol=zeros(6,1);
dsol(1)=-1+cos(sol(3));
dsol(2)=sin(sol(3));
dsol(3)=sol(4)/(E*I);
dsol(4)=-sol(5);
dsol(5)=-sol(6)*dsol(3);
dsol(6)=sol(5)*dsol(3);
end

