function f3 = fonction3(M0,Rn0,Rt0,C0)
    global N f
    [~,sol] = f(M0,Rn0,Rt0);
    f3 = sol(N+1,4)-C0;
end