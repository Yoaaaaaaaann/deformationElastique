# DeformationElastique



## Getting started

Ce programme permet de calculer et d'afficher la déformation subie par une lame élastique soumise à :
Son propore poids, des forces d'extrimité tangentielles et normales, un couple d'extrémité et des forces linéiques.

Il a été réalisé dans la cadre d'un projet de 2eme année à l'INSA Lyon.

Le fichier "EtudeComplete" montre la déformation de la lame avec en entrée des force choisies par l'utilisateur tandis que le fichier "Expérience", comme son nom l'indique,
permet de comparer la déformation numérique avec la déformation réelle mesurée sur un reglet en fer.

Parmis les méthode numériques utilisées, la plus importante est la méthode de Newton Raphson.