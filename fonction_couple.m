function dsol = fonction_couple(s,sol)

global M E I

dsol=zeros(3,1);
dsol(1)=-1+cos(sol(3));
dsol(2)=sin(sol(3));
dsol(3)=M/(E*I);

end

